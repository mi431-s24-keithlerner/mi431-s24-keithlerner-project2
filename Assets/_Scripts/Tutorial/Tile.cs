using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tutorial
{
    public class Tile : MonoBehaviour
    {
        public Tile[] upNeighbours;
        public Tile[] rightNeighbours;
        public Tile[] downNeighbours;
        public Tile[] leftNeighbours;
    }
}
