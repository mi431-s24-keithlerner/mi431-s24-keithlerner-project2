using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Serialization;

namespace Creative
{
    public struct WFC_Collapse : IJobParallelFor
    {
        public NativeSlice<WFC_Cell> lowestEntropySlice;
        
        public void Execute(int index)
        {
            // Get assigned cell
            WFC_Cell assignedCell = lowestEntropySlice[index]; 
            
            // Early exit if cell is collapsed already somehow.
            if (assignedCell.collapsed)
            {
                return;
            }
    
            // Pick a weighted random index to collapse to using weighted state options
            int sum = 0;
            foreach (var val in assignedCell.tileWeights)
            {
                sum += val;
            }
            int randIndex = Random.Range(0, sum);
            
            // Assign tile option at weighted index
            sum = 0;
            WFC_Tile randTile = assignedCell.tileOptions[0];
            for (var i = 0; i < assignedCell.tileOptions.Length; i++)
            {
                if (sum >= randIndex)
                {
                    randTile = assignedCell.tileOptions[i];
                    break;
                }
                
                sum += assignedCell.tileWeights[i];
            }

            // Collapse the cell to its random state
            assignedCell.collapsed = true;
            assignedCell.tileOptions = new NativeArray<WFC_Tile>(new WFC_Tile[]{ randTile }, Allocator.Temp);
            assignedCell.tileWeights = new NativeArray<int>(new int[]{ 1 }, Allocator.Temp);
        }
    }
}
