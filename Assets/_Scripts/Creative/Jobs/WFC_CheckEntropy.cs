using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using PMS = ParallelMergeSort;

namespace Creative
{
    public struct WFC_CheckEntropy : IJobParallelFor
    {
        public NativeArray<WFC_Cell> grid;
        public NativeSlice<WFC_Cell> slice;
        
        public void Execute(int index)
        {
            // Sort grid by entropy
            PMS.Sort(grid, Allocator.TempJob);

            // Generate slice of lowest entropy
            int targetEntropy = grid[0].tileOptions.Length;
            int maxIndexExclusive = 0;
            for (int i = 0; i < grid.Length; i++)
            {
                int e = grid[i].tileOptions.Length;
                if (e > targetEntropy)
                {
                    maxIndexExclusive = i;
                    break;
                }
            }
            slice = new NativeSlice<WFC_Cell>(grid, 0, maxIndexExclusive);
        }
    }
}