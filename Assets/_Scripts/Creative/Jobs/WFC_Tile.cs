using System;
using System.Collections.Generic;
using NaughtyAttributes;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Serialization;

namespace Creative
{
    [Serializable]
    public struct WFC_Tile
    {
        public Sprite TileSprite { get; private set; }

        [Expandable]
        [SerializeField] public NativeArray<WFC_Tile> upNeighbors;
        [SerializeField] public NativeArray<int> upWeights;

        [Expandable]
        [SerializeField] public NativeArray<WFC_Tile> rightNeighbors;
        [SerializeField] public NativeArray<int> rightWeights;
        
        [Expandable]
        [SerializeField] public NativeArray<WFC_Tile> downNeighbors;
        [SerializeField] public NativeArray<int> downWeights;
        
        [Expandable]
        [SerializeField] public NativeArray<WFC_Tile> leftNeighbors;
        [SerializeField] public NativeArray<int> leftWeights;
        
        /* ==================================================================================== SUPER RECURSIVE!!!!!!!!
         ====================================================================================== DON'T USE!!!!!!!!!!!!!!
        public WFC_Tile(Tile tile)
        {
            // Prepare to copy data
            int i;
            
            // Copy sprite data
            TileSprite = tile.TileSprite;
            
            // Copy up data
            upNeighbors = new NativeArray<WFC_Tile>(tile.upNeighbors.Count, Allocator.Persistent);
            upWeights = new NativeArray<int>(tile.upNeighbors.Count, Allocator.Persistent);
            i = 0;
            foreach (var weightPair in tile.upNeighbors)
            {
                upNeighbors[i] = new WFC_Tile(weightPair.Key);
                upWeights[i] = weightPair.Value;
                i++;
            }

            // Copy right data
            rightNeighbors = new NativeArray<WFC_Tile>(tile.rightNeighbors.Count, Allocator.Persistent);
            rightWeights = new NativeArray<int>(tile.rightNeighbors.Count, Allocator.Persistent);
            i = 0;
            foreach (var weightPair in tile.upNeighbors)
            {
                rightNeighbors[i] = new WFC_Tile(weightPair.Key);
                rightWeights[i] = weightPair.Value;
                i++;
            }
            
            // Copy down data
            downNeighbors = new NativeArray<WFC_Tile>(tile.downNeighbors.Count, Allocator.Persistent);
            downWeights = new NativeArray<int>(tile.downNeighbors.Count, Allocator.Persistent);
            i = 0;
            foreach (var weightPair in tile.upNeighbors)
            {
                downNeighbors[i] = new WFC_Tile(weightPair.Key);
                downWeights[i] = weightPair.Value;
                i++;
            }
            
            // Copy left data
            leftNeighbors = new NativeArray<WFC_Tile>(tile.leftNeighbors.Count, Allocator.Persistent);
            leftWeights = new NativeArray<int>(tile.leftNeighbors.Count, Allocator.Persistent);
            i = 0;
            foreach (var weightPair in tile.upNeighbors)
            {
                leftNeighbors[i] = new WFC_Tile(weightPair.Key);
                leftWeights[i] = weightPair.Value;
                i++;
            }
        }
        
        public static bool operator ==(WFC_Tile a, WFC_Tile b)
        {
            // Sprite check
            if (a.TileSprite != b.TileSprite) return false;
            
            // For each up neighbor in both tiles
            if (a.upNeighbors.Length != b.upNeighbors.Length) return false;
            for (int i = 0; i < a.upNeighbors.Length; i++)
            {
                if (a.upNeighbors[i] != b.upNeighbors[i]) return false;
            }
            
            return true;
        }
        
        public static bool operator !=(WFC_Tile a, WFC_Tile b)
        {
            return !(a == b);
        }
        */
        
    }
}