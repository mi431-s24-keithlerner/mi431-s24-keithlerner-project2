using System;
using System.Collections;
using System.Collections.Generic;
using Creative;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

public struct WFC_Validate : IJob
{
    public NativeArray<WFC_Tile> tileOptions;
    public NativeArray<int> weightOptions;
    public NativeArray<WFC_Tile> validOptions;

    public void Execute()
    {
        // Validate each current option
        for (int j = tileOptions.Length - 1; j >= 0; j--)
        {
            // Determine if it is not also a valid cell option
            bool valid = false;
            for (int k = 0; k < validOptions.Length; k++)
            {
                if (tileOptions[j].TileSprite ==
                    validOptions[k].TileSprite)
                {
                    valid = true;
                    break;
                }
            }

            // If element is not in valid cell options
            if (!valid)
            {
                // == Reconstruct the current options array without the element ==
                // Shift elements after the index to the left
                for (int i = j + 1; i < tileOptions.Length; i++)
                {
                    tileOptions[i - 1] = tileOptions[i];
                }

                // Create a resized array
                NativeArray<WFC_Tile> resizedOptions = 
                    new NativeArray<WFC_Tile>(tileOptions.Length - 1, Allocator.Temp);
                for (int i = 0; i < resizedOptions.Length; i++)
                {
                    resizedOptions[i] = tileOptions[i];
                }

                // Set the referenced array to the new, resized array
                tileOptions = resizedOptions;

                // == Reconstruct the weight array without the current element ==
                // Shift elements after the index to the left
                for (int k = j + 1;
                     k < weightOptions.Length;
                     k++)
                {
                    weightOptions[k - 1] =
                        weightOptions[k];
                }

                // Create a resized array
                NativeArray<int> resizedWeights = new NativeArray<int>(
                        weightOptions.Length - 1,
                        Allocator.TempJob);
                for (int k = 0; k < resizedWeights.Length; k++)
                {
                    resizedWeights[k] = weightOptions[k];
                }

                // Set the referenced array to the new, resized array
                weightOptions = resizedWeights;
            }
        }
    }
}
