using System.Collections;
using System.Collections.Generic;
using AYellowpaper.SerializedCollections;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using Unity.Mathematics;
using UnityEngine.Serialization;

namespace Creative
{
    public class WFC_WaveFunction : MonoBehaviour
    {

        [Tooltip("X and Y make up dimensions, Z and W are unused components.")]
        public Vector2Int dimensions;

        private int Area => dimensions.x * dimensions.y;

        private int VectorToAreaCoord(int x, int y) => x + y * dimensions.x;
        
        public NativeArray<WFC_Tile> tileSet;
        
        [ReadOnly] public NativeArray<WFC_Cell> dataGrid;
        [ReadOnly] public SpriteRenderer[] srGrid;

        public GameObject cellPrefab;
        public WFC_Cell cellTemplate;
        
        // Start is called before the first frame update
        void Start()
        {
            // Generate game objects
            if (transform.childCount > 0 ||
                transform.childCount != Area)
            {
                NewGridCells();
            }
            
            // Generate data
            NewData();
            
            // Apply data to game objects
            
            
            // Dispose of any native array
            dataGrid.Dispose();
        }

        #region GameObjectGrid
        
            private void GenerateGridCells()
            {
                srGrid = new SpriteRenderer[Area];
                for (int y = 0; y < dimensions.y; y++)
                {
                    for (int x = 0; x < dimensions.x; x++)
                    {
                        srGrid[VectorToAreaCoord(x, y)] = Instantiate(cellPrefab,
                            new Vector3(x - dimensions.x / 2f,
                                y - dimensions.y / 2f, 0), Quaternion.identity,
                            transform).GetComponent<SpriteRenderer>();
                    }
                }
                
                Debug.Log("Generated Grid Cells");
            }

            private void DestroyGridCells()
            {
                var transArray = transform.GetComponentsInChildren<Transform>();
                for (var index = transArray.Length - 1; index >= 0 ; index++)
                {
                    DestroyImmediate(transArray[index]);
                }
                
                Debug.Log("Destroyed Grid Cells");
            }

            private void NewGridCells()
            {
                // Clear any existing grid cells
                if (transform.childCount > 0)
                {
                    DestroyGridCells();
                }
                    
                // Generate new grid cells
                GenerateGridCells();
            }

        #endregion

        #region Data

            private void GenerateData()
            {
                dataGrid = new NativeArray<WFC_Cell>(Area, Allocator.TempJob,
                    NativeArrayOptions.UninitializedMemory);
                for (int i = 0; i < Area; i++)
                {
                    dataGrid[i] = cellTemplate;
                }
                
                // Core Loop
                // Once per cell in area
                for (int i = 0; i < Area; i++)
                {
                    // == Check entropy of grid ==
                    WFC_CheckEntropy checkEntropy = new WFC_CheckEntropy();
                    checkEntropy.grid = dataGrid;
                    JobHandle entropyJobHandle = checkEntropy.Schedule(1, 1);
                    //entropyJobHandle.Complete(); // Not sure if I need this line? I don't think I do
                    
                    // == Collapse minimum entropy cells from checkEntropy's slice ==
                    WFC_Collapse collapse = new WFC_Collapse();
                    collapse.lowestEntropySlice = checkEntropy.slice;
                    JobHandle collapseJobHandle = collapse.Schedule(checkEntropy.slice.Length, 64, entropyJobHandle);
                    //collapseJobHandle.Complete(); // Not sure if I need this line? I don't think I do
                    
                    // == Propagate to neighbors ==
                    // For each neighbor direction (0-3 : up, right, down, left)
                    for (byte j = 0; j < 4; j++)
                    {
                        // Propagate
                        WFC_PropagateToNeighbor propagate =
                            new WFC_PropagateToNeighbor();
                        propagate.grid = dataGrid;
                        propagate.tileBaseOptions = tileSet;
                        propagate.neighborDirection = j;
                        JobHandle propagateJobHandle = propagate.Schedule(dataGrid.Length, 64, collapseJobHandle);
                        propagateJobHandle.Complete(); // Not sure if I need this line? I think I do
                    }
                }
            }

            private void DestroyData()
            {
                dataGrid.Dispose();
            }

            private void NewData()
            {
                if (dataGrid.Length > 0 && dataGrid.Length != Area)
                {
                    DestroyData();
                }
                
                GenerateData();
            }

        #endregion
        
    }
}

