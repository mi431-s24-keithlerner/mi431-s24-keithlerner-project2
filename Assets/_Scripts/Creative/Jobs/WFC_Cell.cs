using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Serialization;

namespace Creative
{
    public struct WFC_Cell : IComparable<WFC_Cell>
    {
        public bool collapsed;
        public NativeArray<WFC_Tile> tileOptions;
        public NativeArray<int> tileWeights;

        public int CompareTo(WFC_Cell other)
        {
            return tileOptions.Length.CompareTo(other.tileOptions.Length);
        }
    }
}
