using System;
using Unity.Jobs;
using Unity.Collections;

namespace Creative
{
    public struct WFC_PropagateToNeighbor : IJobParallelFor
    {
        public NativeArray<WFC_Cell> grid;
        public NativeArray<WFC_Tile> tileBaseOptions;
        public NativeArray<WFC_Tile> tileValidOptions;
        public byte neighborDirection;
        
        public void Execute(int index)
        {
            // Get assigned grid cell
            WFC_Cell assignedCell = grid[index];
            
            // For each remaining tile option in assigned cell
            for (var i = 0; i < assignedCell.tileOptions.Length; i++)
            {
                WFC_Tile wfcT = assignedCell.tileOptions[i];
                // Get the index of possibleOption in baseOptions
                int baseIndex = 0;
                while (baseIndex < tileBaseOptions.Length)
                {
                    if (wfcT.TileSprite ==
                        tileBaseOptions[baseIndex].TileSprite)
                        break;

                    baseIndex++;
                }

                // Get a native array of valid neighbor tiles based on desired neighbor direction
                tileValidOptions = neighborDirection switch
                {
                    0 => tileBaseOptions[baseIndex].upNeighbors,
                    1 => tileBaseOptions[baseIndex].rightNeighbors,
                    2 => tileBaseOptions[baseIndex].downNeighbors,
                    3 => tileBaseOptions[baseIndex].leftNeighbors,
                    _ => tileBaseOptions
                };

                // Validate neighbor propagation
                WFC_Validate validate = new WFC_Validate();
                validate.tileOptions = assignedCell.tileOptions;
                validate.validOptions = tileValidOptions;
                validate.weightOptions = assignedCell.tileWeights;
                JobHandle validateJobHandle = validate.Schedule();
                validateJobHandle.Complete();
            }
        }
    }
}

