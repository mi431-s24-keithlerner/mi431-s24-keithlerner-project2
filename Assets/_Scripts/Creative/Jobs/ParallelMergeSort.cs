using Unity.Collections;
using Unity.Jobs;

public static class ParallelMergeSort
{
    public static NativeArray<T> Sort<T>(NativeArray<T> array, Allocator allocator) where T : struct, System.IComparable<T>
    {
        var tempArray = new NativeArray<T>(array.Length, allocator);
        array.CopyTo(tempArray);

        ParallelMergeSortJob<T> mergeSortJob = new ParallelMergeSortJob<T>
        {
            Data = tempArray,
            Result = array
        };
        var handle = mergeSortJob.Schedule();

        handle.Complete();
        tempArray.Dispose();

        return array;
    }

    struct ParallelMergeSortJob<T> : IJob where T : struct, System.IComparable<T>
    {
        public NativeArray<T> Data;
        public NativeArray<T> Result;

        public void Execute()
        {
            MergeSort(Data, Result, 0, Data.Length - 1);
        }

        void MergeSort(NativeArray<T> inputArray, NativeArray<T> outputArray, int start, int end)
        {
            if (start < end)
            {
                int mid = (start + end) / 2;
                NativeArray<T> tempArray = new NativeArray<T>(inputArray.Length, Allocator.Temp);

                MergeSort(inputArray, tempArray, start, mid);
                MergeSort(inputArray, tempArray, mid + 1, end);
                Merge(tempArray, outputArray, start, mid, end);

                tempArray.Dispose();
            }
        }

        void Merge(NativeArray<T> inputArray, NativeArray<T> outputArray, int start, int mid, int end)
        {
            int leftIndex = start;
            int rightIndex = mid + 1;
            int outputIndex = start;

            while (leftIndex <= mid && rightIndex <= end)
            {
                if (inputArray[leftIndex].CompareTo(inputArray[rightIndex]) <= 0)
                {
                    outputArray[outputIndex++] = inputArray[leftIndex++];
                }
                else
                {
                    outputArray[outputIndex++] = inputArray[rightIndex++];
                }
            }

            while (leftIndex <= mid)
            {
                outputArray[outputIndex++] = inputArray[leftIndex++];
            }

            while (rightIndex <= end)
            {
                outputArray[outputIndex++] = inputArray[rightIndex++];
            }

            for (int i = start; i <= end; i++)
            {
                inputArray[i] = outputArray[i];
            }
        }
    }
}