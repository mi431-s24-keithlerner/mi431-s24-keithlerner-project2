using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace Creative
{
    public class WaveFunction : MonoBehaviour
    {
        [Header("General")]
        public int dimensions;
        public Tile[] tileSet;
        public GameObject cellPrefab;

        [Header("Debugging")] 
        public bool doDebug;

        public KeyCode stepKey = KeyCode.Space;
        [Range(0, .5f)]
        public float gridBuildTimeStep = .01f;
        
        [Space]
        
        [ReadOnly] [SerializeField] private List<Cell> gridComponents;
        
        private int iterations;
        private Dictionary<Tile, int> weightedTileSet;
        
        void Awake()
        {
            weightedTileSet = TileHelpers.TileArrayToDict(tileSet);
            
            InitializeGrid();
            
            // Start cycle of checking entropy until full collapse
            StartCoroutine(CheckEntropy());
        }
    
        private void InitializeGrid()
        {
            gridComponents = new List<Cell>();
            
            for (int y = 0; y < dimensions; y++)
            {
                for (int x = 0; x < dimensions; x++)
                {
                    GameObject go = Instantiate(cellPrefab, transform.position + new Vector3(-x + dimensions, y, 0), Quaternion.identity);
                    go.transform.SetParent(transform);
                    Cell c = go.GetComponent<Cell>();
                    c.SetTileOptions(weightedTileSet);
                    gridComponents.Add(c);
                }
            }
        }
    
        private IEnumerator CheckEntropy()
        {
            // Create copy of grid
            List<Cell> tempGrid = new List<Cell>(gridComponents);
            
            // Perform quick sort such that cell with least amount of options is in index 0
            tempGrid.RemoveAll(c => c.collapsed);
            tempGrid.Sort((a, b) => a.tileOptions.Count - b.tileOptions.Count);
            
            // Determine cells with matching least amount of options 
            int arrLength = tempGrid[0].tileOptions.Count;
            int stopIndex = default;
            for (int i = 0; i < tempGrid.Count; i++)
            {
                if (tempGrid[i].tileOptions.Count > arrLength)
                {
                    stopIndex = i;
                    break;
                }
            }
            
            // Remove cells with more options than cells with least amount of options
            if (stopIndex > 0)
            {
                tempGrid.RemoveRange(stopIndex, tempGrid.Count - stopIndex);
            }

            if (doDebug)
            {
                if (gridBuildTimeStep == 0)
                {
                    yield return new WaitUntil(() => Input.GetKey(stepKey));
                }
                else
                {
                    yield return new WaitForSeconds(gridBuildTimeStep);                    
                }
            }
            else
            {
                yield return null;
            }
    
            // Perform collapse cell on matching low entropy 
            CollapseCell(tempGrid);
        }
    
        private void CollapseCell(List<Cell> collapsableOptions)
        {
            // Pick a random cell from collapsable options
            int randIndex = Random.Range(0, collapsableOptions.Count);
            Cell cellToCollapse = collapsableOptions[randIndex];
    
            // Pick a weighted random state to collapse to
            int sum = 0;
            foreach (var val in cellToCollapse.tileOptions.Values)
            {
                sum += val;
            }
            randIndex = Random.Range(0, sum);
            sum = 0;
            Tile randTile = tileSet[0];
            foreach (var tileOption in cellToCollapse.tileOptions)
            {
                Debug.Log(tileOption.Key.name);
                
                if (sum >= randIndex) break;

                randTile = tileOption.Key;

                sum += tileOption.Value;
            }
            
            // Collapse the cell to its random state
            cellToCollapse.Collapse(randTile); // This will automatically update the cell sprite
            
            // Propagate collapse through grid
            PropagateGrid();
            
            // Collapse another cell if number of cells collapsed has not exceeded total cell count
            iterations++;
            if (iterations < dimensions * dimensions)
            {
                StartCoroutine(CheckEntropy());
            }
        }
    
        private void PropagateGrid()
        {
            List<Cell> newGenerationCell = new List<Cell>(gridComponents);
    
            for (int y = 0; y < dimensions; y++)
            {
                for (int x = 0; x < dimensions; x++)
                {
                    int index = x + y * dimensions;
                    if (gridComponents[index].collapsed)
                    {
                        newGenerationCell[index] = gridComponents[index];
                    }
                    else
                    {
                        List<Tile> cellOptions = tileSet.ToList();
                        List<Tile> validOptions = new List<Tile>();
                        Cell c;
                        
                        // Up
                        if (y > 0)
                        {
                            c = gridComponents[x + (y - 1) * dimensions];
    
                            PropagateToNeighbor(c, cellOptions, validOptions, Tile.NeighborDirection.Up);
                        }
                        
                        // Right
                        if (x < dimensions - 1)
                        {
                            c = gridComponents[x + 1 + y * dimensions];
    
                            PropagateToNeighbor(c, cellOptions, validOptions, Tile.NeighborDirection.Right);
                        }
                        
                        // Down
                        if (y < dimensions - 1)
                        {
                            c = gridComponents[x + (y + 1) * dimensions];
    
                            PropagateToNeighbor(c, cellOptions, validOptions, Tile.NeighborDirection.Down);
                        }

                        // Left
                        if (x > 0)
                        {
                            c = gridComponents[x - 1 + y * dimensions];
    
                            PropagateToNeighbor(c, cellOptions, validOptions, Tile.NeighborDirection.Left);
                        }
                        
                        // Recreate cell
                        newGenerationCell[index].SetTileOptions(TileHelpers.TileListToDict(cellOptions));
                    }
                }
            }
    
            gridComponents = newGenerationCell;
        }
        
        private void PropagateToNeighbor(Cell c, List<Tile> cellRemainingOptions, 
            List<Tile> potentialValidOptions, Tile.NeighborDirection neighborDirection)
        {
            // For each tile state in possible options of cell c
            foreach (Tile possibleOption in c.tileOptions.Keys)
            {
                // Get the index of this Tile possibleOption in the tileSet array
                int valOption = Array.FindIndex(tileSet, obj => obj == possibleOption);
                
                // Get a dict of valid tiles with weights based on desired neighbor direction
                Dictionary<Tile, int> validTilesDict = neighborDirection switch
                {
                    Tile.NeighborDirection.Up =>    tileSet[valOption].upNeighbors,
                    Tile.NeighborDirection.Right => tileSet[valOption].rightNeighbors,
                    Tile.NeighborDirection.Down =>  tileSet[valOption].downNeighbors,
                    Tile.NeighborDirection.Left =>  tileSet[valOption].leftNeighbors,
                    _ => new Dictionary<Tile, int>()
                };
                
                // Convert dict to list using weights
                List<Tile> validTiles = new();
                foreach (var pair in validTilesDict) // for each tile type
                {
                    for (int i = 0; i < pair.Value; i++) // add one instance to the list per weight given
                    {
                        validTiles.Add(pair.Key);
                    }
                }
    
                // Add valid options to list of valid options
                potentialValidOptions = potentialValidOptions.Concat(validTiles).ToList();
            }
            
            // Validate the potential options
            Validate(cellRemainingOptions, potentialValidOptions);
        }
    
        private static void Validate(List<Tile> cellRemainingOptions, List<Tile> validOptions)
        {
            // For each option
            for (int x = cellRemainingOptions.Count - 1; x >= 0; x--)
            {
                // Remove element if it is not in the list of valid options
                var element = cellRemainingOptions[x];
                if (!validOptions.Contains(element))
                {
                    cellRemainingOptions.RemoveAt(x);
                }
            }
        }
    }
}
