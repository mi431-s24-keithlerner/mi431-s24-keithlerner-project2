using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Serialization;

namespace Creative
{
    public class Cell : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer sr;
        
        public bool collapsed;
        public Dictionary<Tile, int> tileOptions;
    
        private void Start()
        {
            if (sr == null)
            {
                sr = GetComponent<SpriteRenderer>();
            }
        }

        public void SetTileOptions(Dictionary<Tile, int> tiles)
        {
            tileOptions = tiles;

            if (collapsed) return;
            
            sr.sprite = null;
        }
        
        public void Collapse(Tile t)
        {
            collapsed = true;
            tileOptions = new Dictionary<Tile, int> { { t, 1 } };
            foreach (var pair in tileOptions)
            {
                t = pair.Key;
                break;
            }
            
            sr.sprite = t.TileSprite;
    
            gameObject.name = t.name.ToUpper();
        }
    }
}
