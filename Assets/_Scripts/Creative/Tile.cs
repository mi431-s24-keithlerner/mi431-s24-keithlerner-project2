using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AYellowpaper.SerializedCollections;
using NaughtyAttributes;
using UnityEditor.U2D;
using UnityEngine;
using UnityEngine.Serialization;

namespace Creative
{
    [CreateAssetMenu(menuName = "WFC/Tile", fileName = "NewTile")]
    public class Tile : ScriptableObject
    {
        [field: SerializeField] public Sprite TileSprite { get; private set; }
        
        [field: Space] 
        
        public SerializedDictionary<Tile, int> upNeighbors;
        public SerializedDictionary<Tile, int> rightNeighbors;
        public SerializedDictionary<Tile, int> downNeighbors;
        public SerializedDictionary<Tile, int> leftNeighbors;
        
        public enum NeighborDirection { Up, Right, Down, Left }
        
        /*
        public static implicit operator WFC_Tile(Tile tile)
        {
            return new WFC_Tile(tile.TileSprite, tile.UpNeighbors, tile.RightNeighbors, tile.DownNeighbors, tile.LeftNeighbors);
        }
        */
    }

    public static class TileHelpers
    {
        public static Tile[] ToArray(this Dictionary<Tile, int> target)
        {
            int sum = 0;
            foreach (var pair in target)
            {
                sum += pair.Value;
            }

            Tile[] tAr = new Tile[sum];
            sum = 0;
            foreach (var pair in target)
            {
                for (int i = 0; i < pair.Value; i++)
                {
                    tAr[sum] = pair.Key;
                    sum++;
                }
            }

            return tAr.ToArray();
        }
        
        public static Dictionary<Tile, int> TileListToDict(List<Tile> tiles)
        {
            Dictionary<Tile, int> wts = new();
            foreach (Tile t in tiles)
            {
                foreach (var pair in t.upNeighbors)
                {
                    if (wts.ContainsKey(t))
                    {
                        wts[t] += pair.Value;
                    }
                    else
                    {
                        wts[t]  = pair.Value;
                    }
                }
                
                foreach (var pair in t.rightNeighbors)
                {
                    if (wts.ContainsKey(t))
                    {
                        wts[t] += pair.Value;
                    }
                    else
                    {
                        wts[t]  = pair.Value;
                    }
                }
                
                foreach (var pair in t.downNeighbors)
                {
                    if (wts.ContainsKey(t))
                    {
                        wts[t] += pair.Value;
                    }
                    else
                    {
                        wts[t]  = pair.Value;
                    }
                }
                
                foreach (var pair in t.leftNeighbors)
                {
                    if (wts.ContainsKey(t))
                    {
                        wts[t] += pair.Value;
                    }
                    else
                    {
                        wts[t]  = pair.Value;
                    }
                }
            }

            return wts;
        }

        public static Dictionary<Tile, int> TileArrayToDict(Tile[] tiles)
        {
            Dictionary<Tile, int> wts = new();
            foreach (Tile t in tiles)
            {
                foreach (var pair in t.upNeighbors)
                {
                    if (wts.ContainsKey(t))
                    {
                        wts[t] += pair.Value;
                    }
                    else
                    {
                        wts[t]  = pair.Value;
                    }
                }
                
                foreach (var pair in t.rightNeighbors)
                {
                    if (wts.ContainsKey(t))
                    {
                        wts[t] += pair.Value;
                    }
                    else
                    {
                        wts[t]  = pair.Value;
                    }
                }
                
                foreach (var pair in t.downNeighbors)
                {
                    if (wts.ContainsKey(t))
                    {
                        wts[t] += pair.Value;
                    }
                    else
                    {
                        wts[t]  = pair.Value;
                    }
                }
                
                foreach (var pair in t.leftNeighbors)
                {
                    if (wts.ContainsKey(t))
                    {
                        wts[t] += pair.Value;
                    }
                    else
                    {
                        wts[t]  = pair.Value;
                    }
                }
            }

            return wts;
        }
    }
}
