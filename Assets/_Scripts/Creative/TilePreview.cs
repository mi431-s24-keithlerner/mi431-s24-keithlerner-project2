using UnityEngine;
using UnityEditor;

namespace Creative
{
    [CustomPreview(typeof(Tile))]
    public class TilePreview : ObjectPreview
    {
        public override bool HasPreviewGUI()
        {
            return true;
        }
    
        public override void OnPreviewGUI(Rect r, GUIStyle background)
        {
            // Getting things from target works better when it is hard cast to its type
            Tile tile = target as Tile;
            
            // Return if tile is null or sprite is null
            if (tile == null || tile.TileSprite == null) return;
            
            // Calculate the rectangle to draw the sprite.
            var spriteRect = new Rect(r);
    
            // Calculate the aspect ratio of the sprite.
            float spriteAspectRatio = tile.TileSprite.rect.width / tile.TileSprite.rect.height;
    
            // Calculate the maximum possible width and height for the sprite while maintaining its aspect ratio.
            float maxWidth = r.width;
            float maxHeight = r.height;
            if (spriteAspectRatio > 1f)
            {
                maxHeight = maxWidth / spriteAspectRatio;
            }
            else
            {
                maxWidth = maxHeight * spriteAspectRatio;
            }
    
            // Center the sprite within the preview area.
            spriteRect.width = maxWidth;
            spriteRect.height = maxHeight;
            spriteRect.x = r.x + (r.width - spriteRect.width) / 2f;
            spriteRect.y = r.y + (r.height - spriteRect.height) / 2f;
    
            // Draw the sprite preview.
            GUI.DrawTextureWithTexCoords(spriteRect, tile.TileSprite.texture, CalculateTexCoords(tile.TileSprite), true);
        }
        
        /// <summary>
        /// Calculate the texture coordinates of a sprite
        /// </summary>
        /// <param name="sprite"> Sprite to calculate texture coordinates of. </param>
        /// <returns> A rect with corrected texture coordinates. </returns>
        private Rect CalculateTexCoords(Sprite sprite)
        {
            Rect texCoords = sprite.textureRect;
            texCoords.x /= sprite.texture.width;
            texCoords.width /= sprite.texture.width;
            texCoords.y /= sprite.texture.height;
            texCoords.height /= sprite.texture.height;
            return texCoords;
        }
    }
}
